'use strict';
const express = require('express');
const router = express.Router();
const tournament = require('../../src/services/tournaments/index');

router.get('/panel', (req, res, next) => {;
  tournament.getTournaments(req.body.decoded)
    .then(result => res.send(result), next);
});

router.get('/quiniela/:idSeason/:idWeek', (req, res, next) => {
  req.body.IdSeason = req.params.idSeason;
  req.body.MatchNumber = req.params.idWeek;
  tournament.getMatches(req.body)
    .then(result => res.send(result), next);
});

router.get('/quinielas/:idSeason', (req, res, next) => {
  req.body.IdSeason = req.params.idSeason;
  tournament.getAllMatches(req.body)
    .then(result => res.send(result), next);
});

module.exports = router;