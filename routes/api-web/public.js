'use strict';
const express = require('express');
const router  = express.Router();
const login   = require('../../src/services/login/index');

router.post('/login', (req, res, next) => {
  req.body.Admin = true;
  req.body.LoginType = 1;
  login.adminLogin(req.body).then(result => res.send(result), next);
});

module.exports = router;