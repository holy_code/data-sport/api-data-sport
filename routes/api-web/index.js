'use strict';
const routeManager = {};
routeManager.public = require('./public');
routeManager.panel = require('./panel');
module.exports = routeManager;