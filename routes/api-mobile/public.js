'use strict';
const express = require('express');
const router  = express.Router();
const sign_up = require('../../src/services/sign_up/index');
const login   = require('../../src/services/login/index');

router.post('/login', (req, res, next) => {
  req.body.Admin = false;
  req.body.LoginType = req.body.Password ? 1 : req.body.IdGoogle ? 2 :
    req.body.IdFacebook ? 3 : 0; 
  login.userLogin(req.body).then(result => res.send(result), next);
});

router.post('/sign_up', (req, res, next) => {
  sign_up.postRegister(req.body).then(result => res.send(result), next);
});

router.get('/recover_account', (req, res) => {
    res.status(200).send('Ruta para recuperar cuenta');
});

module.exports = router;