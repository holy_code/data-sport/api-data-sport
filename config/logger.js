'use strict';

const winston             = require('winston');
const expressWinston      = require('express-winston-2');
const cloudWatchTransport = require('winston-cloudwatch');

const optionsFormatConsole = {
  format : winston.format.combine(
    winston.format.simple(),
    winston.format.colorize(true),
    winston.format.json(),
    winston.format.timestamp({
      format : 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.printf(
      info => `[ ${ info.timestamp } ] - ${ info.level } - ${ info.message }`
    )
  )
}

const optionsFormatFile = {
  format :  winston.format.combine(
    winston.format.timestamp({ format : 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.printf(
      info => `[ ${ info.timestamp } ] - ${ info.level } - ${ info.message }`
    ),
  ) 
}

const debugLogger = winston.createLogger({
  format : optionsFormatFile.format,
  transports : [
    new winston.transports.File({
      maxsize  : 5120000,
      maxFiles : 5,
      filename : `${ __dirname }/../logs/log-api.log` 
    })
  ]
})

const configureWinstonTransports = () => {
  winston.remove(winston.transports.Console);
  process.env.DEV === '1' ? addDebugConsole() : addInfoConsole();
}

const addDebugConsole = () => {
  debugLogger.add(
    new winston.transports.Console({
      level  : 'debug',
      format : optionsFormatConsole.format 
    })
  )
}

const addInfoConsole = () => {
  debugLogger.add(
    new winston.transports.Console({
      level  : 'info',
      format : optionsFormatConsole.format 
    })
  )
}

const configureExpressWinston = () => {
  expressWinston.requestWhitelist.push('body');
  expressWinston.responseWhitelist.push('body');
  expressWinston.bodyBlacklist.push('Contrasena', 'StoredContrasena');
}

const loggerExpressWinston = {
  winstonInstance : debugLogger,
  level : 'info',
  msg : 'HTTP - {{ req.method }} {{ req.url }}',
  colorStatus : true,
}

const loggerExpressDebugWinston = {
  winstonInstance : debugLogger,
  level : 'info',
  expressFormat : true,
  colorStatus : true,
  meta : false,
}

configureWinstonTransports();
configureExpressWinston();

module.exports = {
  debugLogger,
  expressLogger : expressWinston.logger(loggerExpressWinston),
  expressConsoleLogger : expressWinston.logger(loggerExpressDebugWinston)
}