'use strict';
const mySqlModel      = require('mysql');
const host            = process.env.DBHOST;
const user            = process.env.DBUSER;
const password        = process.env.DBPASSWORD;
const database        = process.env.DBNAME;
const connectionLimit = process.env.DBCONNECTION_LIMIT;
const acquireTimeout  = process.env.DBACQUIRE_TIMEOUT;

module.exports = {
  Key: process.env.TOKEN_KEY,
  host,
  user,
  password,
  database,
  connectionPool : mySqlModel.createPool({
    connectionLimit,
    host,
    user,
    password,
    database,
    dateStrings : 'date',
    debug : false,
    acquireTimeout,   
  }),
}