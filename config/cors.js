'use strict';

const environmet = process.env.PROD === '1' ? true : false;
const whiteList  = [
  'http://localhost:3000',
  'http://localhost:4000',
  'http://localhost:5000'
];

const corsOptionsDelegate = function (req, callback) {
  const corsOptions = { origin : true };
  whiteList.indexOf(req.header('Origin')) !== -1 ? 
    corsOptions.origin = environmet : corsOptions.origin = !environmet;
  callback(null, corsOptions)
}

module.exports = { corsOptionsDelegate }