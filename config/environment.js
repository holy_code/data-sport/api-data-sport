'use strict';
const env = 'development';
const dotenv = require('dotenv');

const loadEnvVariables = () => {
  switch (env) {
    case 'development': dotenv.config({path: './config/.env.development' });
      break;
    case 'local': dotenv.config({ path: './config/.env.local' });
      break;
    case 'production': dotenv.config({ path: './config/.env.production' });
      break;
    default: dotenv.config({ path: './config/.env.development' });
  }
};

if(env) loadEnvVariables();
dotenv.config({ path: './config/.env.development' });

const PORT = process.env.PORT;
const CTX  = env;

module.exports = {
  PORT,
  CTX
};