const validator = require('validator');
const moment    = require('moment');
const validate  = {};

validate.isBuildle = identifier => {  
  const resp = (identifier.match(/\W/g) || []).length;
  return resp === 3 || resp === 2 ? true : false;
};

validate.isPoints = (points) => {
  return points > 0 && points <= 99 ? true : false;
};

validate.isEmail = (email) => {
  const reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if(reg.test(email)) return true;
  return false;
};

validate.isDateWithin = function (date, hour) {
  if ((new Date().getTime() - date.getTime()) <= hour) return true;
  return false;
};

validate.isMySqlDate = function (date) {
  const regex = /^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(?:( [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/m;
  return regex.test(date);
};

validate.isPositiveInt = stringToCheck => {
  const str = stringToCheck.toString();  
  return validator.isInt(str, { min: 0 }) && (str + 0 > 0);
};

validate.isBoolean = stringToCheck => validator.isBoolean(stringToCheck);

validate.objectHasProperty = (objectToCheck, property) =>
  Object.hasOwnProperty.call(objectToCheck, property) && objectToCheck[property] !== undefined;

validate.isEmpty = stringToCheck => {
  const str = stringToCheck + '';
  return validator.isEmpty(str.trim());
};

validate.isString = stringToCheck => (typeof stringToCheck !== 'string');

validate.isIntinRange = (stringToCheck, range) => {
  const str = stringToCheck.toString();
  return validator.isInt(str, range);
};

validate.isUrl = stringToCheck => {
  const str = stringToCheck.toString();
  return validator.isURL(str);
};

validate.isNumeric = stringToCheck => {
  const str = stringToCheck.toString();
  return !validator.isNumeric(str);
};

validate.hasImageExtension = stringToCheck => {
  const str = stringToCheck.toString().toLowerCase();
  return str.match(/\.(jpeg|jpg|gif|png|svg)$/) != null;
};

validate.isValidInGroup = (number, accepted) => {
  if (accepted.indexOf(Number(number)) > -1) return true;
  return false;
};

validate.isHexadecimal = stringToCheck => validator.isHexadecimal(stringToCheck.trim());

validate.isHexColor = stringToCheck => validator.isHexColor(stringToCheck.trim());

validate.isUUID = stringToCheck => validator.isUUID(stringToCheck.trim());

validate.isNonEmptyArray = array => (!Array.isArray(array) || array.length < 1);

validate.isBase64 = stringToCheck => validator.isBase64(stringToCheck);

validate.isValidMask = (baseMask, providedMask) => (baseMask & parseInt(providedMask, 2)) === baseMask;

validate.isBefore = (start, end) => moment(start).isBefore(end);

module.exports = validate;