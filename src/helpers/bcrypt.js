'use strict';
const crypt = require('bcryptjs');
const q = require('q');
const crypter = {};
const cycles = 10;

crypter.encrypt = text => {
  const deferred = q.defer();
  crypt.genSalt(cycles, (saltError, salt) => {
    if (saltError) deferred.reject(saltError);
    if (text) {
      crypt.hash(text, salt, (hashError, hash) => {
        if (hashError) deferred.reject(hashError);
        else deferred.resolve(hash);
      });
    } else {
      deferred.resolve('');
    }
  });
  return deferred.promise;
};

crypter.compare = (text, hash) => {
  const deferred = q.defer();
  crypt.compare(text, hash, (error, isEqual) => {
    if (error) deferred.reject(error);
    else deferred.resolve(isEqual);
  });
  return deferred.promise;
};

module.exports = crypter;