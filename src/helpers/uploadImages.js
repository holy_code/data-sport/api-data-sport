'use strict';
const Buffer = require('buffer').Buffer;
const path   = require('path');
const fs     = require('fs');
const images = {};

/**
 * @param {string} base64str
 * @param {string} filename
 */
 images.decode_base64 = (base64str, filename) => {
  let buffer = new Buffer.from(base64str.replace(/^data:image\/\w+;base64,/, ''), 'base64');
  let base = path.resolve('src/assets/img/' + filename + '.jpg');
  
  fs.writeFile(path.join(base), buffer, function(error) {
    if (error) {
      return error;
    } else {
      return base;
    }
  });
}

module.exports = images;