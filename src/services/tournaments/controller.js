'use strict';
const model = require('./model');
const tournament = {};

tournament.getTournaments = () => model.getAll(); 

tournament.getMatches = panel => {
  return model.getMatchesWeek(panel);
}

tournament.getAllMatches = idSeason => {
  return model.getAllSeasonMatches(idSeason).then(orderMatchweek);
}

const orderMatchweek = matchweek => {
  const weeksList = [];
  matchweek.data.forEach(match => weeksList.push(match.Matchweek));
  matchweek.data = weeksList
  return matchweek;

}

module.exports = tournament;