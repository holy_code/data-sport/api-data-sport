'use strict';
const controller = require('./controller');
//const validator  = require('./helpers/validator');
const tournament = {};

tournament.getTournaments = () => controller.getTournaments().catch(err => err);

tournament.getMatches = panel => {
  return controller.getMatches(panel).catch(err => err);
}

tournament.getAllMatches = season => {
  return controller.getAllMatches(season.IdSeason).catch(err => err);
}

module.exports = tournament;