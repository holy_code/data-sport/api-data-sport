'use strict';
const help        = require('../../helpers/help');
const tournament  = {};
const tables      = {
  "seasons": "tra_seasons",
  "tournament": "cat_tournaments",
  "calendar": "tra_calendar_season",
  "teams": "cat_teams"
}

tournament.getAll = () => {
  return help.d$().query(`
    SELECT
      season.IdSeason,
      season.NameSeason,
      tournament.IdTournament,
      tournament.NameTournament,
      tournament.LogoTournament,
      tournament.IdFederation,
      tournament.IdConfederation,
      MatchNumber AS Matchweek
    FROM ${tables.seasons} AS season
      INNER JOIN ${tables.tournament} AS tournament ON season.IdTournament = tournament.IdTournament
      INNER JOIN ${tables.calendar} AS calendar ON MatchNumber = (
      SELECT 
        MIN(MatchNumber)
      FROM ${tables.calendar} AS Matchweek
      WHERE DATE_FORMAT(NOW(), '%Y-%m-%d') <= DATE_FORMAT(Matchweek.MatchDate, '%Y-%m-%d')
      AND Matchweek.IdSeason = season.IdSeason
    )
    WHERE season.Active = 1 GROUP BY season.IdSeason;
  `);
}

tournament.getMatchesWeek = panel => {  
  return help.d$().query(`
  SELECT
    calendar.IdCalendarSeason,
    teamLocal.IdTeam AS IdLocal,
    teamLocal.NameTeam AS NameLocal,
    teamLocal.AcronymTeam AS AcronymTeamLocal,
    teamLocal.LogoTeam AS LogoTeamLocal,
    teamVisitor.IdTeam AS IdVisitor,
    teamVisitor.NameTeam AS NameVisit,
    teamVisitor.AcronymTeam AS AcronymTeamVisitor,
    teamVisitor.LogoTeam AS LogoTeamVisitor,
    teamLocal.Stadium,
    DATE_FORMAT(calendar.MatchDate, '%b %d/%H:%i %p') AS MatchDate,
    calendar.MatchStatus,
    calendar.MatchNumber
  FROM ${tables.calendar} AS calendar
    INNER JOIN ${tables.teams} AS teamLocal 
      ON teamLocal.IdTeam = calendar.IdLocalTeam
    INNER JOIN ${tables.teams} AS teamVisitor
      ON teamVisitor.IdTeam = calendar.IdVisitingTeam
  WHERE calendar.IdSeason = (?) AND calendar.MatchNumber = (?);
  `,[panel.IdSeason, panel.MatchNumber]);
}

tournament.getAllSeasonMatches = idSeason => {
  return help.d$().query(`
  SELECT 
	  season.MatchNumber AS Matchweek
  FROM ${tables.calendar} AS season
  WHERE season.IdSeason = (?)
  GROUP BY season.MatchNumber;
  `, [idSeason]);
}
module.exports = tournament;