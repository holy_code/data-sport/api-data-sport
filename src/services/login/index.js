'use strict';
const controller = require('./controller');
const validator  = require('./helpers/validator');
const service    = {};

service.adminLogin = user => {
  validator.Login(user);
  return controller.verifyAccount(user)
    .then(response => controller.login(response))
    .catch(error => error)
}

service.userLogin = user => {
  user.LoginType === 1 ? validator.Login(user) : validator.SocialNetworks(user);
  return controller.verifyAccount(user)
    .then(response => controller.login(response))
    .catch(error => error)
}
module.exports = service;