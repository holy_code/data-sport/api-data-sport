'use strict';
const throwloginErrors = require('../../../tools/errorFactory');
const loginErrors = {};

loginErrors.accountDoesNotExist = () => {
  throwloginErrors(0, 'La cuenta de Quiniela King no existe', [], 10);
}

loginErrors.wrongPassword = () => {
  throwloginErrors(0, 'Las contraseñas no coinciden', [], 20);
}

loginErrors.blockedAccount = () => {
  throwloginErrors(0, 'Cuenta esta bloqueada', [], 30);
}

loginErrors.unknownCustomer = () => {
  throwloginErrors(0, 'Cliente desconocido', [], 40);
}

module.exports = loginErrors;