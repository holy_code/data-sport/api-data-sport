'use strict';
const validator = require('../../../tools/validate');
const validate  = {};

validate.Login = user => {
  validator.checkEmailRequired(user, 'Email');
  validator.checkRequiredString(user, 'Password');
};

validate.SocialNetworks = user => {
  validator.checkEmailRequired(user, 'Email');
  validator.checkString(user, 'IdGoogle');
  validator.checkString(user, 'IdFacebook');
};

module.exports = validate; 