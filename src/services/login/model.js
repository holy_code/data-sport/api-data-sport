'use strict';
const help        = require('../../helpers/help');
const cleanObject = require('../../tools/utils').cleanObject;
const model       = {};
const tables      = {
  "users" : "tra_users"
}

model.check_account = account => {
  const operator = account.Admin ? '<= 2' : '=== 3';
  return help.d$().query(`
    SELECT * FROM ${ tables.users } 
    WHERE Email = (?) AND IdUserType ${ operator };`, [account.Email]);
  }

module.exports = model;