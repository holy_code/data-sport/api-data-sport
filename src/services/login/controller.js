'use strict';
const model       = require('./model');
const jwt         = require('jsonwebtoken');
const config      = require('../../../config/database');
const bcrypt      = require('../../helpers/bcrypt');
const loginErrors = require('./helpers/loginErrors');
const controller  = {};

const createToken = data => jwt.sign(data, config.Key, { expiresIn: '30d' });
const verifyPassword = user => bcrypt.compare(user.Equate, user.Password);

const normalizedProfile = data => {  
  const normalize = {
    IdUser: data.IdUser,
    Name: data.Name,
    LastName: data.LastName || '',
    CompleteName : data.Name + ' ' + data.LastName ? data.LastName : '',
    Mobile: data.Mobile,
    Email: data.Email,
    Password: data.Password,
    Identifier: data.Identifier,
    Photo: data.Photo,
    Birthday: data.Birthday,
    IdGoogle: data.IdGoogle,
    IdFacebook: data.IdFacebook,
    IdUserType: data.IdUserType,
    Active: data.Active
  }
  return normalize;
}

controller.verifyAccount = user => {
  return model.check_account(user).then(response => {
    if(user.LoginType === 0) loginErrors.unknownCustomer();
    if(response.data.length === 0) loginErrors.accountDoesNotExist();
    if(response.data[0].Active != 1) loginErrors.blockedAccount();
    switch (user.LoginType) {
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;  
    }
    response.data[0].Equate = user.Password;
    return response.data[0];
  })
}

controller.login = async user => {
  const result = await verifyPassword(user);
  if(result === false) loginErrors.wrongPassword();
  const profile = normalizedProfile(user);
  profile.Token = createToken(profile);  
  return { success : 1, message : 'Inicio de sesión correcto', data : profile };
}

module.exports = controller;