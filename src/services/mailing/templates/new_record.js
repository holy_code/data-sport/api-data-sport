'use strict';
const defaultImage = 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT8Cuyq7AdKhhmAL6ceV-s8oLT630oo9uMOWPjH994GigCH5S3q';
const TEMPLATE = data => {
  
  const emailInformation = {
    Photo : data.user.Photo || defaultImage,
    Name  : data.user.Name,
    From  : data.smtp.auth.user,
    To    : data.user.Email,
    Bcc   : 'mauro.sanchez.s@hotmail.com'
  }
  delete data.user;
  
  const mailing = {
    from    : `KG <${ emailInformation.From }>`,
    to      : `${ emailInformation.To }`,
    bcc     : `${ emailInformation.Bcc }`,
    subject : 'REGISTRO', 
    text    : '',
    html    : template(emailInformation) 
  }  
  
  return { mailing, smtp: data.smtp }
};

const template = data => {
  return `
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Document</title>
  </head>
      <body style="background-color: rgb(217, 220, 223);">
          <table width=75% align=center style="background-color: white;">
              <tr> 
                  <td width=100% align=center>
                      <div>
                          <img width=50% src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/5826815/30715396_367819970383533_1449824880538277736_n.jpg?width=64" alt="Imagen_banner">
                      </div>
                  </td>
              </tr>
              <tr> 
                  <td width=100% align=center>
                      <div>
                          <img style="margin-left: 10px; margin-right: 10px; width:80px; height:80px; border-radius:150px;" width=50% heigth=16% src="${ data.Photo }" alt="Imagen_banner">
                      </div>
                  </td>
              </tr>
              <tr>
                  <td align=center>
                      <p>
                          <font face="Helvetica" color=grey>
                              <h1>¡Hola ${ data.Name }!</h1>
                          </font>
                      </p>
                  </td>
              </tr> 
              <tr>
                  <td align=center>
                      <p>
                          <font size="3" face="Helvetica" color=grey>
                              <h3 style="margin-left: 14px; margin-right: 14px;">Recuerda usar tu correo y la contraseña para poder acceder a tu cuenta</h3>
                          </font></p>
                  </td>
              </tr>
              <tr>
                  <td align=center>
                      <p>
                          <font face="Helvetica" color=grey>
                              <h4>¿Necesitas ayuda? soporte@holy-code.com</h4>
                          </font>
                      </p>
                  </td>
              </tr>
              <tr style="background-color: #FFFFFF;">
                  <td align=center>
                      <div align=center >
                          <br>
                          <img width=25% src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/5826815/30715396_367819970383533_1449824880538277736_n.jpg?width=64" alt="logo">
                          <p align=center>
                              <font face="Helvetica" color=grey style="width:auto; align-items:center">
                                  <h5 align=center style="margin-left: 10px; margin-right: 10px;">Si quieres tener una aplicación personalizada para que tus colabores participen y puedan obtener
                                    recompensas no dudes en contactarnos en holy-code.com  o  al Teléfono: 33 2159 4848
                                  </h5>
                              </font>
                          </p>
                          <a href="#" target="_blank" style="color:#6EBF6A">Aviso de Privacidad</a>
                          <div heigth></div>
                      </div>
                  </td>
              </tr>
          </table>
        </body>
  
  </html>
  `
}

module.exports = {
  TEMPLATE
}