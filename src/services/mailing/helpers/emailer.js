'use strict';
const nodemailer = require('nodemailer');
const main = {}

main.sendMail = async data => {
  const transporter = nodemailer.createTransport(data.smtp);
  return await transporter.sendMail(data.mailing);
}

module.exports = main;
