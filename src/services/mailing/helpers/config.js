'use strict';
const emailerConfig = {};

emailerConfig.mailerTransport = () => {
  const mailerTransport = {
    pool: true,
    host: 'smtp.gmail.com',
    port: 587,
    rateDelta: 1000,
    maxConnections: 3,
    auth: {
       user: 'mauro.sanchez.simental@gmail.com',
       pass: 'ssm10674',
    },
    secureConnection: false,
    tls: {
      ciphers: 'SSLv3',
      rejectUnauthorized: false,
    },
  }  
  return mailerTransport;
}

module.exports = emailerConfig;