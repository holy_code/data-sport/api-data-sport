'use strict';
const smtpClient = require('./helpers/config').mailerTransport;
const templates  = require('./templates/index');
const emailer    = require('./helpers/emailer');
const service    = {};

service.sendMail = async user => {
  const emailToSend = smtpClient();
  const email = typeTemplate(emailToSend, user);
  await emailer.sendMail(email);
  return user;
}

const typeTemplate = (smtp, user) => {
  switch (user.Template) {
    case 'register_new': user = templates.NEW_RECORD({ smtp, user });
      break;
    case 'register_new_admin': user = templates.NEW_RECORD_ADMIN({ smtp, user });
      break;
    default: 
      break;
  }  
  return user;
}

module.exports = service;