'use strict';
const controller   = require('./controller');
const validator    = require('./helpers/validator');
const emailService = require('../mailing/index');
const sign_up      = {};

sign_up.postRegister = user => {
  validator.Register(user);  
  return controller.verifyAccount(user)
    .then(response => controller.generateProfile(response))
    .then(response => emailService.sendMail(response))
    .then(response => controller.login(response))
    .catch(error => error)
}

module.exports = sign_up;