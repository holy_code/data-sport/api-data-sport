'use strict';
const model        = require('./model');
const jwt          = require('jsonwebtoken');
const config       = require('../../../config/database');
const encrypt      = require('../../tools/encrypt');
const signupErrors = require('./helpers/signupErrors');
const resgiter     = {};
const createToken = data => jwt.sign(data, config.Key, { expiresIn: '90d' });

const normalizedAccount = account => {
  const normalize = {
    CompleteName: account.CompleteName,
    IdUser: account.IdUser,
    Name: account.Name,
    LastName: account.LastName,
    Mobile: account.Mobile,
    Email: account.Email,
    Identifier: account.Identifier,
    Photo: account.Photo,
    Birthday: account.Photo,
    IdGoogle: account.IdGoogle,
    IdFacebook: account.IdFacebook,
    IdUserType: account.IdUserType,
    Active: account.Active,
    CreationDate: account.CreationDate,
    ModificationDate: account.ModificationDate
  }
  return normalize;
}

resgiter.verifyAccount = user => {
  return model.check_email(user.Email).then(response => {
    if(response.data.length === 0) return user;
    signupErrors.theUserExists();
  })
}

resgiter.generateProfile = async user => {
  if(user.Identifier === 0) user = await verifyPassword(user);
  return await model.sign_up(user).then(res => { 
    return res.success > 0 ? getUser(res.data.insertId) : signupErrors.createProfile();
  });
}

resgiter.login = async account => {
  const normalize = normalizedAccount(account);
  normalize.Token = createToken(normalize);
  if(normalize.Token.length === 0) signupErrors.generateToken();
  return { success: 1, message: "Registro Exitoso", data: normalize, code: 50 }; 
}

const verifyPassword = async user => {
  if(user.Password === user.ConfirmPassword) {
    return await encrypt(user.Password).then(response => {
      delete user.ConfirmPassword;
      user.Password = response;
      return user;
    })
  }
  signupErrors.wrongPassword();
}

const getUser = async id => await model.get_user(id).then(res => res.data[0]);

module.exports = resgiter;