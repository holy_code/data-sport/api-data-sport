'use strict';
const throwsignupErrors = require('../../../tools/errorFactory');
const signupErrors = {};

signupErrors.theUserExists = () => {
  throwsignupErrors(0, 'La cuenta de Quiniela King ya existe', [], 10);
}

signupErrors.wrongPassword = () => {
  throwsignupErrors(0, 'Las contraseñas no coinciden', [], 20);
}

signupErrors.createProfile = () => {
  throwsignupErrors(0, 'Problema al crear el perfil', [], 30);
}

signupErrors.generateToken = () => {
  throwsignupErrors(1, 'Ya puedes iniciar sesión', [], 40);
}

module.exports = signupErrors;