'use strict';
const validator = require('../../../tools/validate');
const validate  = {};

validate.Register = user => {
  validator.checkRequiredString(user, 'Name');
  validator.checkRequiredString(user, 'LastName');
  validator.checkEmailRequired(user, 'Email');
  validator.checkString(user, 'Password');
  validator.checkString(user, 'ConfirmPassword');
  validator.checkRequiredIntInRange(user, "Identifier", { min: 0, max: 2 });
  validator.checkUrl(user, 'Photo');
};

module.exports = validate; 