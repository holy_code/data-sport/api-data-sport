'use strict';
const help        = require('../../helpers/help');
const cleanObject = require('../../tools/utils').cleanObject;
const register    = {};
const tables      = {
  "users" : "tra_users"
}

const normalizeRecord = user => {
  const normalize = {
    Name        : user.Name,
    LastName    : user.LastName,
    Mobile      : user.Mobile || undefined,
    Email       : user.Email,
    Password    : user.Password || undefined,
    Identifier  : user.Identifier,
    Photo       : user.Photo || undefined,
    Birthday    : user.Birthday || undefined,
    IdGoogle    : user.IdGoogle || undefined,
    IdFacebook  : user.IdFacebook || undefined,
    CreationDate: help.d$().now()
  };
  cleanObject(normalize);
  return normalize;
}

register.get_user = id => {
  return help.d$().query(`
    SELECT CONCAT(user.Name, ' ', user.LastName) AS CompleteName, 'register_new' AS Template,
      user.* FROM ${ tables.users } AS user 
    WHERE user.IdUser = (?);`, [id])
}

register.check_email = email => {
  return help.d$().query(`
    SELECT IdUser FROM ${ tables.users } WHERE Email = (?);`, [email])
}

register.sign_up = user => {
  return help.d$().insert(tables.users, normalizeRecord(user));
}

module.exports = register;