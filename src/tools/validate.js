'use strict';
const validator       = require('../helpers/validations');
const errors          = require('./customErrors');
const objectValidator = {};

const ischeckRankPoints = (objectToCheck, idName) => {
  return !validator.isPoints(objectToCheck[idName]);
};

const isPropertyEmpty = (objectToCheck, propertyName) => {
  return objectToCheck[propertyName] === undefined || objectToCheck[propertyName] === ''
};

const removeEmptyProperty = (objectToCheck, propertyName) => {
  if (isPropertyEmpty(objectToCheck, propertyName)) delete objectToCheck[propertyName];
};

const hasInvalidId = (objectToCheck, idName) => {
  if (validator.objectHasProperty(objectToCheck, idName)) return !validator.isPositiveInt(objectToCheck[idName]);
  return true;
};

const hasInvalidPositieInt = (objectToCheck, idName) => {
  return validator.objectHasProperty(objectToCheck, idName) && !validator.isPositiveInt(objectToCheck[idName])
};

const hasRequiredPositieInt = (objectToCheck, idName) => {
  if (validator.objectHasProperty(objectToCheck, idName)) return !validator.isPositiveInt(objectToCheck[idName]);
  return true;
};

const hasNoProperty = (objectToCheck, idName) => {
  return !validator.objectHasProperty(objectToCheck, idName)
};

const hasEmptyString = (objectToCheck, propertyName) => {
  return validator.objectHasProperty(objectToCheck, propertyName) && validator.isEmpty(objectToCheck[propertyName])
};

const hasRequiredEmptyString = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return validator.isEmpty(objectToCheck[propertyName]);
  return true;
};

const hasInvalidIntInRange = (objectToCheck, propertyName, range) => {
  return validator.objectHasProperty(objectToCheck, propertyName) && !validator.isIntinRange(objectToCheck[propertyName], range)
};

const hasRequiredInvalidIntInRange = (objectToCheck, propertyName, range) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return !validator.isIntinRange(objectToCheck[propertyName], range);
  return true;
};

const hasInvalidRequiredBase64 = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return !validator.isBase64(objectToCheck[propertyName]);
  return true;
};

const hasInvalidBoolean = (objectToCheck, propertyName) => {
  return validator.objectHasProperty(objectToCheck, propertyName) && !validator.isBoolean(objectToCheck[propertyName])
};

const hasInvalidRequiredBoolean = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return !validator.isBoolean(objectToCheck[propertyName]);
  return true;
};

const hasInvalidUrl = (objectToCheck, propertyName) => {
  return validator.objectHasProperty(objectToCheck, propertyName)
    && !validator.isUrl(objectToCheck[propertyName])
};

const hasInvalidImageExtension = (objectToCheck, propertyName) => {
  return validator.objectHasProperty(objectToCheck, propertyName)
    && !validator.hasImageExtension(objectToCheck[propertyName])
};

const hasInvalidRequiredImageExtension = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return !validator.hasImageExtension(objectToCheck[propertyName]);
  return true;
};

const hasInvalidDate = (objectToCheck, propertyName) => (
  validator.objectHasProperty(objectToCheck, propertyName) && !validator.isMySqlDate(objectToCheck[propertyName])
);

const hasInvalidRequiredDate = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return !validator.isMySqlDate(objectToCheck[propertyName]);
  return true;
};

const hasInvalidUUID = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) {
    return !(validator.isHexadecimal(objectToCheck[propertyName]) || validator.isUUID(objectToCheck[propertyName]));
  }
  return true;
};

const hasInvalidHecadecimal = (objectToCheck, propertyName) => (
  validator.objectHasProperty(objectToCheck, propertyName) && !validator.isHexadecimal(objectToCheck[propertyName])
);

const hasRequiredHecadecimal = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return !validator.isHexadecimal(objectToCheck[propertyName]);
  return true;
};

const hasInvalidHexaColor = (objectToCheck, propertyName) => (
  validator.objectHasProperty(objectToCheck, propertyName) && !validator.isHexColor(objectToCheck[propertyName])
);

const hasRequiredHexaColor = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return !validator.isHexColor(objectToCheck[propertyName]);
  return true;
};

const hasEmptyArray = (objectToCheck, propertyName) => {
  if (validator.objectHasProperty(objectToCheck, propertyName)) return validator.isNonEmptyArray(objectToCheck[propertyName]);
  return true;
};

objectValidator.checkId = (objectToCheck, idName) => {
  if (hasInvalidId(objectToCheck, idName)) errors.isWholeNumber(idName);
};

objectValidator.checkPositiveInt = (objectToCheck, idName) => {
  removeEmptyProperty(objectToCheck, idName);
  if (hasInvalidPositieInt(objectToCheck, idName)) errors.isWholeNumber(idName);
};

objectValidator.checkRequirePositiveInt = (objectToCheck, idName) => {
  if (hasRequiredPositieInt(objectToCheck, idName)) errors.isWholeNumber(idName);
};

objectValidator.checkRankPoints = (objectToCheck, idName, range = { 'min' : 0, 'max' : 99 }) => {
  if (ischeckRankPoints(objectToCheck, idName)) errors.integerRange(idName, range);
};

objectValidator.checkString = (objectToCheck, propertyName) => {
  removeEmptyProperty(objectToCheck, propertyName);
  if (hasEmptyString(objectToCheck, propertyName)) errors.emptyField(propertyName);
};

objectValidator.checkRequiredString = (objectToCheck, propertyName) => {
  if (hasRequiredEmptyString(objectToCheck, propertyName)) errors.emptyField(propertyName);
};

objectValidator.checkIntInRange = (objectToCheck, propertyName, range) => {
  removeEmptyProperty(objectToCheck, propertyName);
  if (hasInvalidIntInRange(objectToCheck, propertyName, range)) errors.integerRange(propertyName, range);
};

objectValidator.checkRequiredIntInRange = (objectToCheck, propertyName, range) => {
  if (hasRequiredInvalidIntInRange(objectToCheck, propertyName, range)) errors.integerRange(propertyName, range);
};

objectValidator.checkBoolean = (objectToCheck, propertyName) => {
  if (hasInvalidBoolean(objectToCheck, propertyName)) errors.isBoolean(propertyName);
};

objectValidator.checkRequiredBoolean = (objectToCheck, propertyName) => {
  if (hasInvalidRequiredBoolean(objectToCheck, propertyName)) errors.isBoolean(propertyName);
};

objectValidator.checkImageUrl = (objectToCheck, propertyName) => {
  removeEmptyProperty(objectToCheck, propertyName);
  if (hasInvalidUrl(objectToCheck, propertyName)) errors.isURL(propertyName);
  if (hasInvalidImageExtension(objectToCheck, propertyName)) errors.isImg(propertyName);
};

objectValidator.checkRequiredImageUrl = (objectToCheck, propertyName) => {
  if (hasInvalidUrl(objectToCheck, propertyName)) errors.isURL(propertyName);
  if (hasInvalidRequiredImageExtension(objectToCheck, propertyName)) errors.isImg(propertyName);
};

objectValidator.checkOpcionalImageUrl = (objectToCheck, propertyName) => {
  removeEmptyProperty(objectToCheck, propertyName);
  if (hasInvalidUrl(objectToCheck, propertyName)) errors.isURL(propertyName);
  if (hasInvalidImageExtension(objectToCheck, propertyName)) errors.isImg(propertyName);
};

objectValidator.checkDate = (objectToCheck, propertyName) => {
  if (hasInvalidDate(objectToCheck, propertyName)) errors.isDate(propertyName);
};

objectValidator.checkRequiredDate = (objectToCheck, propertyName) => {
  if (hasInvalidRequiredDate(objectToCheck, propertyName)) errors.isDate(propertyName);
};

objectValidator.checkUUID = (objectToCheck, propertyName) => {
  if (hasInvalidUUID(objectToCheck, propertyName)) errors.isUUID(propertyName);
};

objectValidator.checkEmailRequired = (objectToCheck, propertyName) => {
  if (!validator.isEmail(objectToCheck.Email, propertyName)) errors.isEmail(objectToCheck.Email);
};

objectValidator.checkEmail = (objectToCheck, propertyName) => {
  removeEmptyProperty(objectToCheck, propertyName);
  if (!hasRequiredEmptyString(objectToCheck, propertyName)) objectValidator.checkEmailRequired(objectToCheck, propertyName);
};

objectValidator.checkBuildleRequired = (objectToCheck, propertyName) => {
  if (!validator.isBuildle(objectToCheck.identifier, propertyName)) errors.isBuildle(objectToCheck.identifier);
};

objectValidator.checkNonEmptyArray = (objectToCheck, propertyName) => {
  if (hasEmptyArray(objectToCheck, propertyName)) errors.emptyArray(propertyName);
};

objectValidator.checkProperty = (objectToCheck, propertyName) => {
  if (hasNoProperty(objectToCheck, propertyName))errors.doesNotExist(propertyName);
};

objectValidator.checkRequiredBase64 = (objectToCheck, propertyName) => {
  if (hasInvalidRequiredBase64(objectToCheck, propertyName)) errors.base64(propertyName);
};

objectValidator.checkRequiredHexadecimal = (objectToCheck, propertyName) => {
  if (hasRequiredHecadecimal(objectToCheck, propertyName)) errors.isHexa(propertyName); 
};

objectValidator.checkHexadecimal = (objectToCheck, propertyName) => {
  if (hasInvalidHecadecimal(objectToCheck, propertyName)) errors.isHexa(propertyName);
}; 

objectValidator.checkHexaColor = (objectToCheck, propertyName) => {
  if (hasInvalidHexaColor(objectToCheck, propertyName)) errors.isHexaColor(propertyName);
};

objectValidator.checkRequiredHexaColor = (objectToCheck, propertyName) => {
  if (hasRequiredHexaColor(objectToCheck, propertyName)) errors.isHexaColor(propertyName);
};

objectValidator.checkRequiredUrl = (objectToCheck, propertyName) => {
  if (hasNoProperty(objectToCheck, propertyName)) errors.isURL(propertyName);
  if (hasInvalidUrl(objectToCheck, propertyName)) errors.isURL(propertyName);
};

objectValidator.checkUrl = (objectToCheck, propertyName) => {
  removeEmptyProperty(objectToCheck, propertyName);
  if (hasInvalidUrl(objectToCheck, propertyName)) errors.isURL(propertyName);
};

objectValidator.checkIfStartDateIsBeforeEndDate = (objectToCheck, startDate, endDate) => {
  removeEmptyProperty(objectToCheck, startDate);
  removeEmptyProperty(objectToCheck, endDate);
  if (hasNoProperty(objectToCheck, startDate) || hasNoProperty(objectToCheck, endDate)) return;
  if (!validator.isBefore(objectToCheck[startDate], objectToCheck[endDate])) errors.beforeOrAfterDate(startDate, endDate);
};

objectValidator.checkEmailArray = objectToCheck => {
  if (!validator.isEmail(objectToCheck.Email)) errors.isEmail(objectToCheck.Email);
};

objectValidator.checkRequiredXMLProperty = (objectToCheck, propertyName) => {
  if (hasNoProperty(objectToCheck, propertyName)) errors.isXML(propertyName);
};

module.exports = objectValidator;