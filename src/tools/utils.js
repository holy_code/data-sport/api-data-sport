'use strict';
const util = {};

util.cleanObject = object => {
  Object.keys(object).forEach(propertyName => {
    if (object[propertyName] === undefined) delete object[propertyName];
  });
};

module.exports = util;