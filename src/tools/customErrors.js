'use strict';
const throwCustomError = require('./errorFactory');

const isEmail = (propertyName) => {
  throwCustomError('isEmail', propertyName + ' The email is not valid', 200);
};

const isBuildle = (propertyName) => {
  throwCustomError('isBuildle', propertyName + ' Incorrect format', 200);
};

const isWholeNumber = (propertyName) => {
  throwCustomError('number', propertyName + ' it is not an integer greater than 0', 200);
};

const isBoolean = (propertyName) => {
  throwCustomError('boolean', propertyName + ' it must be Boolean (0, 1)', 200);
};

const isURL = (propertyName) => {
  throwCustomError('url', propertyName + ' it must be a URL', 200);
};

const isImg = (propertyName) => {
  throwCustomError('img', propertyName + ' must have image extension', 200);
};

const isDate = (propertyName) => {
  throwCustomError('date', propertyName + ' must have format YYYY-MM-DD', 200);
};

const isUUID = (propertyName) => {
  throwCustomError('uuid', propertyName + ' it must be a valid UUID', 200);
};

const isXML = (propertyName) => {
  throwCustomError('xml', propertyName + ' Badly formed XML', 200);
};

const isHexa = (propertyName) => {
  throwCustomError('hexa', propertyName + ' it is not hexadecimal', 200);
};

const isHexaColor = (propertyName) => {
  throwCustomError('hexacolor', propertyName + ' it is not a hexadecimal color', 200);
};

const emptyField = (propertyName) => {
  throwCustomError('string', propertyName + ' it must not be empty', 200);
};

const integerRange = (propertyName, range) => {
  throwCustomError('range', propertyName + ' it must be an integer in the range [' + range.min + '-' + range.max + ']', 200);
}

const emptyArray = (propertyName) => {
  throwCustomError('emptyArray', propertyName + ' it is not a non-empty arrangement');
}; 

const doesNotExist = (propertyName) => {
  throwCustomError('doesNotExist', propertyName + ' does not exist');
};          

const base64 = (propertyName) => {
  throwCustomError(propertyName + ' its not a base64 string');
}

const beforeOrAfterDate = (startDate, endDate) => {
  throwCustomError(startDate + ' it does not start before ' + endDate);
};

const passwordError = () => {
  throwCustomError('The password does not match.');
};

module.exports = {
  isEmail,
  isBuildle,
  isWholeNumber,
  isBoolean,
  isURL,
  isImg,
  isDate,
  isUUID,
  isXML,
  isHexa,
  isHexaColor,
  emptyField,
  integerRange,
  emptyArray,
  doesNotExist,
  base64,
  beforeOrAfterDate,
  passwordError
}