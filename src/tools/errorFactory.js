'use strict';
const CustomError = require('./errorHandling');

module.exports = (success, message, data, code) => {
  throw new CustomError(success, message, data, code);
};