'use strict';
const bcrypt = require('../helpers/bcrypt');
const encrypt = async password => await bcrypt.encrypt(password);
module.exports = encrypt;