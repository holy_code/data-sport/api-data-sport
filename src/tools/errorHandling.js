'use strict';
module.exports = function customError(success, message, data, code) {
  Error.captureStackTrace(this, this.constructor);
  this.success = success || 0;
  this.message = message || 'Default Message';
  this.data    = data    ||	[];
  this.code    = code    ||	0;
}
require('util').inherits(module.exports, Error);