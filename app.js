'use strict';
const VERSION      = '0.0.1'; //ALPHA
const {PORT, CTX}  = require('./config/environment');
const express      = require('express');
const cors         = require('cors');
const configCors   = require('./config/cors').corsOptionsDelegate;
const config       = require('./config/database');
const bodyParser   = require('body-parser');
const jwt          = require('jsonwebtoken');
const helmet       = require('helmet');
const app          = express();
const http         = require('http').Server(app);
const mobileRoutes = require('./routes/api-mobile/index');
const webRoutes    = require('./routes/api-web/index');
const { 
  debugLogger, 
  expressConsoleLogger, 
  expressLogger }  = require('./config/logger');

app.use(cors(configCors));
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressLogger);
app.use(expressConsoleLogger);

const handleError = (error, req, res, next) => {
  debugLogger.error(error);
  if(error.data) {
    error.message = error.data.code || error.message;
    delete error.data.code;
    res.status(400).send(error);
    return;
  }

  const errorResult = {
    success: 0, message: error.message || error, data: []
  }

  const statusCode = error.errorCode || 400;
  res.status(statusCode).send(errorResult);
}

const defaultRoute = (req, res) => {
  res.status(404).send({
    success: 0,
    message: 'Ruta Invalida',
    data: []
  })
}

app.use('/mobile', mobileRoutes.public);
app.use('/web', webRoutes.public);

app.use((req, res, next) => {
  res.setHeader('Cache-Control', 'no-cache');
  const token = req.headers.token;  
  !token ? res.send({
    success: 0,
    message: 'You need a token to access our api, please login.',
    data: []}) :
    jwt.verify(token, config.Key, (error, decoded) => {
      error ? res.status(401).send({
        success: 0,
        message: 'Token expired or invalid, please login once more.', 
        data: []}) :
        req.decoded = decoded;
        req.body.decoded = decoded;
        next();
    });
});

app.use('/web', webRoutes.panel);

app.use(handleError);
app.use(defaultRoute);

http.listen(PORT, () => debugLogger.info('The Data Sport V' + VERSION + ' run ' + CTX + ' port ' + PORT));